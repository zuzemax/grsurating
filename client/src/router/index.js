import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);

const routes = [
  {
    path: "/rating/:id",
    name: "rating",
    component: () => import("../views/Rating"),
    props: true,
  },
  {
    path: "/info/:id",
    name: "info",
    component: () => import("../views/TeacherInfo"),
    props: true,
  },
  {
    path: "/signin",
    name: "SignIn",
    component: () => import("../views/SignIn"),
  },
  {
    path: "/search",
    name: "Search",
    component: () => import("../views/Search"),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  const publicPages = ["/signin"];
  const authRequired = !publicPages.includes(to.path);
  const loggedIn = localStorage.getItem("user");
  if (authRequired && !loggedIn) {
    next("/signin");
  } else {
    next();
  }
});

export default router;
