package by.grsu.rating.api.v1.controller;

import by.grsu.rating.repository.SubCriterionRepository;
import by.grsu.rating.service.TeacherScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeacherRatingSumController {
    @Autowired
    private TeacherScoreService teacherScoreService;
    @Autowired
    private SubCriterionRepository subCriterionRepository;

    @GetMapping("/sum/{userId}")
    public int getSum(@PathVariable("userId") long userId) {
        return teacherScoreService.getById(userId).stream()
                .mapToInt(value -> {
                    Long subCriterionId = value.getId().getSubCriterionId();
                    Integer score = subCriterionRepository.findById(subCriterionId).get().getScore();
                    return score * value.getPoints();
                })
                .reduce(Integer::sum)
                .getAsInt();
    }
}
