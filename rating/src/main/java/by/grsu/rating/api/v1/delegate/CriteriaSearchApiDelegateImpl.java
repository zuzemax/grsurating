package by.grsu.rating.api.v1.delegate;

import by.grsu.rating.api.v1.endpoint.CriteriaSearchApiDelegate;
import by.grsu.rating.model.dto.SearchFilterDto;
import by.grsu.rating.model.dto.SearchResultDto;
import by.grsu.rating.service.TeacherScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CriteriaSearchApiDelegateImpl implements CriteriaSearchApiDelegate {
    @Autowired
    private TeacherScoreService teacherScoreService;

    @Override
    public ResponseEntity<List<SearchResultDto>> searchCriteria(SearchFilterDto searchFilterDto) {
        return ResponseEntity.ok(teacherScoreService.searchByFilter(searchFilterDto));
    }
}
