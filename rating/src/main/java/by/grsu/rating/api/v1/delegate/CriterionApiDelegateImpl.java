package by.grsu.rating.api.v1.delegate;

import by.grsu.rating.api.v1.endpoint.CriterionApiDelegate;
import by.grsu.rating.model.dto.CriterionDto;
import by.grsu.rating.service.CriterionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CriterionApiDelegateImpl implements CriterionApiDelegate {
    @Autowired
    private CriterionService criterionService;

    @Override
    public ResponseEntity<List<CriterionDto>> getAll() {
        List<CriterionDto> criterionDtos = criterionService.getAll();
        return ResponseEntity.ok(criterionDtos);
    }

    @Override
    public ResponseEntity<Void> delete(Long id) {
        criterionService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @Override
    public ResponseEntity<CriterionDto> save(CriterionDto criterionDto) {
        CriterionDto responseBody = criterionService.save(criterionDto);
        return ResponseEntity.ok(responseBody);
    }
}
