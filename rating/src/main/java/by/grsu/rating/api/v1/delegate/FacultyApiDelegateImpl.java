package by.grsu.rating.api.v1.delegate;

import by.grsu.rating.api.v1.endpoint.FacultyApiDelegate;
import by.grsu.rating.model.dto.FacultyDto;
import by.grsu.rating.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacultyApiDelegateImpl implements FacultyApiDelegate {
    @Autowired
    private FacultyService facultyService;

    @Override
    public ResponseEntity<List<FacultyDto>> getAllFaculties() {
        return ResponseEntity.ok(facultyService.getAll());
    }

    @Override
    public ResponseEntity<FacultyDto> saveFaculty(FacultyDto facultyDto) {
        return ResponseEntity.ok(facultyService.save(facultyDto));
    }
}
