package by.grsu.rating.api.v1.delegate;

import by.grsu.rating.api.v1.endpoint.TeacherInfoApiDelegate;
import by.grsu.rating.model.dto.TeacherInfoDto;
import by.grsu.rating.service.TeacherInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherInfoApiDelegateImpl implements TeacherInfoApiDelegate {
    @Autowired
    private TeacherInfoService teacherInfoService;

    @Override
    public ResponseEntity<List<TeacherInfoDto>> getAllTeacherInfos() {
        List<TeacherInfoDto> teacherInfoDtos = teacherInfoService.getAll();
        return ResponseEntity.ok(teacherInfoDtos);
    }

    @Override
    public ResponseEntity<TeacherInfoDto> getTeacherInfo(Long id) {
        TeacherInfoDto teacherInfoDto = teacherInfoService.getById(id);
        return ResponseEntity.ok(teacherInfoDto);
    }

    @Override
    public ResponseEntity<TeacherInfoDto> saveTeacherInfo(TeacherInfoDto teacherInfoDto) {
        TeacherInfoDto responseBody = teacherInfoService.save(teacherInfoDto);
        return ResponseEntity.ok(responseBody);
    }
}
