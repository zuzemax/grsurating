package by.grsu.rating.api.v1.delegate;

import by.grsu.rating.api.v1.endpoint.TeacherScoreApiDelegate;
import by.grsu.rating.model.dto.TeacherScoreDto;
import by.grsu.rating.service.TeacherScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherScoreApiDelegateImpl implements TeacherScoreApiDelegate {
    @Autowired
    private TeacherScoreService teacherScoreService;

    @Override
    public ResponseEntity<List<TeacherScoreDto>> getTeacherScore(Long id) {
        List<TeacherScoreDto> teacherScoreDtos = teacherScoreService.getById(id);
        return ResponseEntity.ok(teacherScoreDtos);
    }

    @Override
    public ResponseEntity<Void> saveTeacherScore(TeacherScoreDto teacherScoreDto) {
        teacherScoreService.save(teacherScoreDto);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
