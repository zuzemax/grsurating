package by.grsu.rating.authentication.payload;

import lombok.Getter;
import lombok.Setter;

import java.util.Collection;

@Getter
@Setter
public class AuthResponse {
    private String token;
    private String type = "Bearer";
    private Long id;
    private String username;
    private String email;
    private Collection<String> roles;

    private AuthResponse(String token, Long id, String username, String email, Collection<String> roles) {
        this.token = token;
        this.id = id;
        this.username = username;
        this.email = email;
        this.roles = roles;
    }

    public static AuthResponse build(String token, Long id, String username, String email, Collection<String> roles) {
        return new AuthResponse(token, id, username, email, roles);
    }
}