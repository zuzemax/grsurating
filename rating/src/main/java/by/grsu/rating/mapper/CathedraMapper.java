package by.grsu.rating.mapper;

import by.grsu.rating.model.dto.CathedraDto;
import by.grsu.rating.model.entity.CathedraEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CathedraMapper {
    CathedraDto toDto(CathedraEntity cathedraEntity);

    CathedraEntity toEntity(CathedraDto cathedraDto);

    List<CathedraDto> toDtoList(List<CathedraEntity> cathedraEntities);

    List<CathedraEntity> toEntityList(List<CathedraDto> cathedraDtos);
}
