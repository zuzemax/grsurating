package by.grsu.rating.mapper;

import by.grsu.rating.model.dto.CriterionDto;
import by.grsu.rating.model.entity.CriterionEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = SubCriterionMapper.class)
public interface CriterionMapper {
    CriterionDto toDto(CriterionEntity criterionEntity);

    CriterionEntity toEntity(CriterionDto criterionDto);

    List<CriterionDto> toDtoList(List<CriterionEntity> criterionEntities);

    List<CriterionEntity> toEntityList(List<CriterionDto> criterionDtos);
}
