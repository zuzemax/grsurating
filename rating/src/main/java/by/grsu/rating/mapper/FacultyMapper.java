package by.grsu.rating.mapper;


import by.grsu.rating.model.dto.FacultyDto;
import by.grsu.rating.model.entity.FacultyEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = CathedraMapper.class)
public interface FacultyMapper {
    FacultyDto toDto(FacultyEntity facultyEntity);

    FacultyEntity toEntity(FacultyDto facultyDto);

    List<FacultyDto> toDtoList(List<FacultyEntity> facultyEntities);

    List<FacultyEntity> toEntityList(List<FacultyDto> facultyDtos);
}
