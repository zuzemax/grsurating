package by.grsu.rating.mapper;

import by.grsu.rating.model.dto.CriterionDto;
import by.grsu.rating.model.dto.SubCriterionDto;
import by.grsu.rating.model.entity.CriterionEntity;
import by.grsu.rating.model.entity.SubCriterionEntity;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface SubCriterionMapper {
    SubCriterionDto toDto(SubCriterionEntity subCriterionEntity);

    SubCriterionEntity toEntity(SubCriterionDto subCriterionDto);

    List<SubCriterionDto> toDtoList(List<SubCriterionEntity> subCriterionEntities);

    List<SubCriterionEntity> toEntityList(List<SubCriterionDto> subCriterionDtos);
}
