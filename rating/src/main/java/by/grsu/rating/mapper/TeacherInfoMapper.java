package by.grsu.rating.mapper;

import by.grsu.rating.model.dto.TeacherInfoDto;
import by.grsu.rating.model.entity.TeacherInfoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeacherInfoMapper {

    @Mapping(target = "cathedra", source = "cathedra.name")
    @Mapping(target = "faculty", source = "cathedra.faculty.name")
    TeacherInfoDto toDto(TeacherInfoEntity teacherInfoEntity);

    @Mapping(target = "cathedra", ignore = true)
    TeacherInfoEntity toEntity(TeacherInfoDto teacherInfoDto);

    List<TeacherInfoDto> toDtoList(List<TeacherInfoEntity> teacherInfoEntities);

    List<TeacherInfoEntity> toEntityList(List<TeacherInfoDto> teacherInfoDtos);
}
