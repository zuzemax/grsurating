package by.grsu.rating.mapper;

import by.grsu.rating.model.dto.SearchResultDto;
import by.grsu.rating.model.dto.TeacherScoreDto;
import by.grsu.rating.model.entity.TeacherInfoEntity;
import by.grsu.rating.model.entity.TeacherScoreEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TeacherScoreMapper {


    TeacherScoreDto toDto(TeacherScoreEntity teacherScoreEntity);

    TeacherScoreEntity toEntity(TeacherScoreDto teacherScoreDto);

    List<TeacherScoreDto> toDtoList(List<TeacherScoreEntity> teacherScoreEntities);

    List<TeacherScoreEntity> toEntityList(List<TeacherScoreDto> teacherScoreDtos);

    List<SearchResultDto> toSearchResultList(List<TeacherScoreEntity> teacherScoreEntities);

    @Mapping(target = "fullName", expression = "java(getFullName(teacherScoreEntity))")
    @Mapping(target = "faculty", source = "user.teacherInfo.cathedra.faculty.name")
    @Mapping(target = "cathedra", source = "user.teacherInfo.cathedra.name")
    @Mapping(target = "criterion", source = "subCriterion.criterion.description")
    @Mapping(target = "subCriterion", source = "subCriterion.description")
    @Mapping(target = "criterionId", source = "subCriterion.criterion.id")
    @Mapping(target = "subCriterionId", source = "subCriterion.id")
    @Mapping(target = "score", source = "points")
    SearchResultDto toSearchResult(TeacherScoreEntity teacherScoreEntity);

    default String getFullName(TeacherScoreEntity entity) {
        TeacherInfoEntity teacherInfo = entity.getUser().getTeacherInfo();
        return String.format("%s %s %s",
                teacherInfo.getLastName(),
                teacherInfo.getFirstName(),
                teacherInfo.getFathersName());
    }
}
