package by.grsu.rating.model.auth;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
    ADMIN, EXPERT, TEACHER;

    @Override
    public String getAuthority() {
        return this.name();
    }
}
