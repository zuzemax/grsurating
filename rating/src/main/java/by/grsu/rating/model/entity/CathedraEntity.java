package by.grsu.rating.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = CathedraEntity.TABLE)
public class CathedraEntity {
    public static final String TABLE = "CATHEDRA";
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_NAME = "NAME";
    public static final String COLUMN_FACULTY_ID = "FACULTY_ID";
    @Id
    @Column(name = COLUMN_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = COLUMN_NAME)
    private String name;
    @ManyToOne(targetEntity = FacultyEntity.class)
    private FacultyEntity faculty;
}
