package by.grsu.rating.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = FacultyEntity.TABLE)
public class FacultyEntity {
    public static final String TABLE = "FACULTY";
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_NAME = "NAME";
    @Id
    @Column(name = COLUMN_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = COLUMN_NAME)
    private String name;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = CathedraEntity.COLUMN_FACULTY_ID, referencedColumnName = COLUMN_ID)
    private List<CathedraEntity> cathedras;
}
