package by.grsu.rating.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = SubCriterionEntity.TABLE_SUB_CRITERION)
public class SubCriterionEntity {
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_DESCRIPTION = "DESCRIPTION";
    public static final String COLUMN_SCORE = "SCORE";
    public static final String COLUMN_CRITERION_ID = "CRITERION_ID";
    public static final String TABLE_SUB_CRITERION = "SUB_CRITERION";
    @Id
    @Column(name = COLUMN_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = COLUMN_DESCRIPTION, nullable = false)
    private String description;
    @Column(name = COLUMN_SCORE, nullable = false)
    private Integer score;
    @ManyToOne(targetEntity = CriterionEntity.class)
    private CriterionEntity criterion;
//    @OneToMany(mappedBy = "subCriterion")
//    private List<UserSubCriterion> userSubCriteria;
}
