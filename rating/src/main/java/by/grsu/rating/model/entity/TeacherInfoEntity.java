package by.grsu.rating.model.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name = TeacherInfoEntity.TABLE)
@Getter
@Setter
public class TeacherInfoEntity {
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_CATHEDRA_ID = "CATHEDRA_ID";
    public static final String COLUMN_DEGREE = "DEGREE";
    public static final String COLUMN_FIRST_NAME = "FIRST_NAME";
    public static final String COLUMN_FATHERS_NAME = "FATHERS_NAME";
    public static final String COLUMN_LAST_NAME = "LAST_NAME";
    public static final String TABLE = "TEACHER_INFO";
    @Id
    @Column(name = COLUMN_ID)
    private Long id;
    @OneToOne
    @PrimaryKeyJoinColumn
    private UserEntity user;
    @ManyToOne(targetEntity = CathedraEntity.class)
    @JoinColumn(name = COLUMN_CATHEDRA_ID)
    private CathedraEntity cathedra;
    @Column(name = COLUMN_DEGREE)
    private String degree;
    @Column(name = COLUMN_FIRST_NAME)
    private String firstName;
    @Column(name = COLUMN_FATHERS_NAME)
    private String fathersName;
    @Column(name = COLUMN_LAST_NAME)
    private String lastName;
}
