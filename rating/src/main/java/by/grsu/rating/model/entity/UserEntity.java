package by.grsu.rating.model.entity;

import by.grsu.rating.model.auth.Role;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = UserEntity.TABLE_USER)
public class UserEntity {
    public static final String COLUMN_ID = "ID";
    public static final String COLUMN_USER_ID = "USER_ID";
    public static final String COLUMN_EMAIL = "EMAIL";
    public static final String COLUMN_PASSWORD = "PASSWORD";
    public static final String COLUMN_USERNAME = "USERNAME";
    public static final String TABLE_USER_ROLE = "USER_ROLE";
    public static final String TABLE_USER = "USER";
    @Id
    @Column(name = COLUMN_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    @Column(name = COLUMN_USERNAME, nullable = false, unique = true)
    private String username;
    @Column(name = COLUMN_PASSWORD, nullable = false)
    private String password;
    @Column(name = COLUMN_EMAIL, nullable = false)
    private String email;
    @Enumerated(EnumType.ORDINAL)
    @ElementCollection(targetClass = Role.class)
    @CollectionTable(name = TABLE_USER_ROLE, joinColumns = @JoinColumn(name = COLUMN_USER_ID, referencedColumnName = COLUMN_ID))
    private Collection<Role> roles;
//    @OneToMany(mappedBy = "user")
//    private List<TeacherScoreEntity> teacherScoreEntities;
    @OneToOne(mappedBy = "user")
    private TeacherInfoEntity teacherInfo;
}
