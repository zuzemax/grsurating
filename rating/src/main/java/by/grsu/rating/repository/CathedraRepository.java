package by.grsu.rating.repository;

import by.grsu.rating.model.entity.CathedraEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CathedraRepository extends JpaRepository<CathedraEntity, Long> {
    CathedraEntity findByName(String name);
}
