package by.grsu.rating.repository;

import by.grsu.rating.model.entity.CriterionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CriterionRepository extends JpaRepository<CriterionEntity, Long> {
}
