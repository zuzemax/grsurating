package by.grsu.rating.repository;

import by.grsu.rating.model.entity.SubCriterionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubCriterionRepository extends JpaRepository<SubCriterionEntity, Long> {
}
