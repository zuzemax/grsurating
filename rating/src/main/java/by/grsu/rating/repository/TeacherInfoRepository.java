package by.grsu.rating.repository;

import by.grsu.rating.model.entity.TeacherInfoEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherInfoRepository extends JpaRepository<TeacherInfoEntity, Long> {
}
