package by.grsu.rating.repository;

import by.grsu.rating.model.entity.TeacherScoreEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TeacherScoreRepository extends JpaRepository<TeacherScoreEntity, TeacherScoreEntity.Key> {

    List<TeacherScoreEntity> findAllByUserId(long userId);

    @Query("select ts from TeacherScoreEntity as ts where (:firstName is null or ts.user.teacherInfo.firstName like :firstName) and " +
            "(:fathersName is null or ts.user.teacherInfo.fathersName like :fathersName) and " +
            "(:lastname is null or ts.user.teacherInfo.lastName like :lastname) and " +
            "(:subCriterionId is null or ts.subCriterion.id = :subCriterionId) and " +
            "(:criterionId is null or ts.subCriterion.criterion.id = :criterionId) and " +
            "(:cathedraId is null or ts.user.teacherInfo.cathedra.id = :cathedraId) and " +
            "(:facultyId is null or ts.user.teacherInfo.cathedra.faculty.id = :facultyId)")
    List<TeacherScoreEntity> findByFilter(@Param("firstName") String firstName, @Param("fathersName") String fathersName,
                                          @Param("lastname") String lastname, @Param("subCriterionId") Long subCriterionId,
                                          @Param("criterionId") Long criterionId, @Param("cathedraId") Long cathedraId,
                                          @Param("facultyId") Long facultyId);
}
