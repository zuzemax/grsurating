package by.grsu.rating.service;

import by.grsu.rating.model.dto.CriterionDto;
import by.grsu.rating.model.dto.SearchFilterDto;
import by.grsu.rating.model.dto.SearchResultDto;

import java.util.List;

public interface CriterionService {
    List<CriterionDto> getAll();

    CriterionDto save(CriterionDto criterionDto);

    void delete(Long id);

    List<CriterionDto> saveAll(List<CriterionDto> criterionDtos);
}
