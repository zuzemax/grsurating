package by.grsu.rating.service;

import by.grsu.rating.model.dto.FacultyDto;

import java.util.List;

public interface FacultyService {
    List<FacultyDto> getAll();

    FacultyDto save(FacultyDto facultyDto);
}
