package by.grsu.rating.service;

import by.grsu.rating.model.dto.TeacherInfoDto;

import java.util.List;

public interface TeacherInfoService {
    List<TeacherInfoDto> getAll();

    TeacherInfoDto save(TeacherInfoDto teacherInfoDto);

    TeacherInfoDto getById(long id);
}
