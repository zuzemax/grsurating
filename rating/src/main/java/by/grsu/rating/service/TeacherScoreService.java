package by.grsu.rating.service;

import by.grsu.rating.model.dto.SearchFilterDto;
import by.grsu.rating.model.dto.SearchResultDto;
import by.grsu.rating.model.dto.TeacherScoreDto;

import java.util.List;

public interface TeacherScoreService {

    List<TeacherScoreDto> getById(long id);

    void save(TeacherScoreDto teacherScoreDto);

    List<SearchResultDto> searchByFilter(SearchFilterDto searchFilterDto);
}
