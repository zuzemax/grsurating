package by.grsu.rating.service.impl;

import by.grsu.rating.mapper.CriterionMapper;
import by.grsu.rating.model.dto.CriterionDto;
import by.grsu.rating.model.dto.SearchFilterDto;
import by.grsu.rating.model.dto.SearchResultDto;
import by.grsu.rating.model.entity.CriterionEntity;
import by.grsu.rating.repository.CriterionRepository;
import by.grsu.rating.service.CriterionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CriterionServiceImpl implements CriterionService {
    @Autowired
    private CriterionRepository criterionRepository;
    @Autowired
    private CriterionMapper criterionMapper;

    @Override
    public List<CriterionDto> getAll() {
        List<CriterionEntity> criterionEntities = criterionRepository.findAll();
        return criterionMapper.toDtoList(criterionEntities);
    }

    @Override
    public CriterionDto save(CriterionDto criterionDto) {
        CriterionEntity criterionEntity = criterionMapper.toEntity(criterionDto);
        CriterionEntity saved = criterionRepository.save(criterionEntity);
        return criterionMapper.toDto(saved);
    }

    @Override
    public void delete(Long id) {
        criterionRepository.deleteById(id);
    }

    @Override
    public List<CriterionDto> saveAll(List<CriterionDto> criterionDtos) {
        List<CriterionEntity> criterionEntities = criterionMapper.toEntityList(criterionDtos);
        List<CriterionEntity> saved = criterionRepository.saveAll(criterionEntities);
        return criterionMapper.toDtoList(saved);
    }
}
