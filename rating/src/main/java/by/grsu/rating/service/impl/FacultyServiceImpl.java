package by.grsu.rating.service.impl;

import by.grsu.rating.mapper.FacultyMapper;
import by.grsu.rating.model.dto.FacultyDto;
import by.grsu.rating.model.entity.FacultyEntity;
import by.grsu.rating.repository.FacultyRepository;
import by.grsu.rating.service.FacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FacultyServiceImpl implements FacultyService {
    @Autowired
    private FacultyRepository facultyRepository;
    @Autowired
    private FacultyMapper facultyMapper;

    @Override
    public List<FacultyDto> getAll() {
        List<FacultyEntity> facultyEntities = facultyRepository.findAll();
        return facultyMapper.toDtoList(facultyEntities);
    }

    @Override
    public FacultyDto save(FacultyDto facultyDto) {
        FacultyEntity facultyEntity = facultyMapper.toEntity(facultyDto);
        FacultyEntity saved = facultyRepository.save(facultyEntity);
        return facultyMapper.toDto(saved);
    }

}
