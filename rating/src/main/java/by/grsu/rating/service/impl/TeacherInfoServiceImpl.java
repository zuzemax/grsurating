package by.grsu.rating.service.impl;

import by.grsu.rating.mapper.TeacherInfoMapper;
import by.grsu.rating.model.dto.TeacherInfoDto;
import by.grsu.rating.model.entity.TeacherInfoEntity;
import by.grsu.rating.repository.CathedraRepository;
import by.grsu.rating.repository.TeacherInfoRepository;
import by.grsu.rating.service.TeacherInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherInfoServiceImpl implements TeacherInfoService {
    @Autowired
    private TeacherInfoRepository teacherInfoRepository;
    @Autowired
    private TeacherInfoMapper teacherInfoMapper;
    @Autowired
    private CathedraRepository cathedraRepository;

    @Override
    public List<TeacherInfoDto> getAll() {
        List<TeacherInfoEntity> teacherInfoEntities = teacherInfoRepository.findAll();
        return teacherInfoMapper.toDtoList(teacherInfoEntities);
    }

    @Override
    public TeacherInfoDto save(TeacherInfoDto teacherInfoDto) {
        TeacherInfoEntity teacherInfoEntity = teacherInfoMapper.toEntity(teacherInfoDto);
        teacherInfoEntity.setCathedra(cathedraRepository.findByName(teacherInfoDto.getCathedra()));
        TeacherInfoEntity saved = teacherInfoRepository.save(teacherInfoEntity);
        return teacherInfoMapper.toDto(saved);
    }

    @Override
    public TeacherInfoDto getById(long id) {
        TeacherInfoEntity teacherInfoEntity = teacherInfoRepository.findById(id).orElseThrow(RuntimeException::new);
        return teacherInfoMapper.toDto(teacherInfoEntity);
    }
}
