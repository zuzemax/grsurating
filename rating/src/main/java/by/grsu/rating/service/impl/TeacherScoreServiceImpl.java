package by.grsu.rating.service.impl;

import by.grsu.rating.mapper.TeacherScoreMapper;
import by.grsu.rating.model.dto.SearchFilterDto;
import by.grsu.rating.model.dto.SearchResultDto;
import by.grsu.rating.model.dto.TeacherScoreDto;
import by.grsu.rating.model.entity.SubCriterionEntity;
import by.grsu.rating.model.entity.TeacherScoreEntity;
import by.grsu.rating.model.entity.UserEntity;
import by.grsu.rating.repository.SubCriterionRepository;
import by.grsu.rating.repository.TeacherScoreRepository;
import by.grsu.rating.repository.UserRepository;
import by.grsu.rating.service.TeacherScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherScoreServiceImpl implements TeacherScoreService {
    @Autowired
    private TeacherScoreRepository teacherScoreRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private SubCriterionRepository subCriterionRepository;
    @Autowired
    private TeacherScoreMapper teacherScoreMapper;

    @Override
    public List<TeacherScoreDto> getById(long id) {
        List<TeacherScoreEntity> teacherScoreEntities = teacherScoreRepository.findAllByUserId(id);
        return teacherScoreMapper.toDtoList(teacherScoreEntities);
    }

    @Override
    public void save(TeacherScoreDto teacherScoreDto) {
        TeacherScoreEntity teacherScoreEntity = teacherScoreMapper.toEntity(teacherScoreDto);
        Long userId = teacherScoreEntity.getId().getUserId();
        UserEntity user = userRepository.findById(userId).get();
        teacherScoreEntity.setUser(user);
        Long subCriterionId = teacherScoreEntity.getId().getSubCriterionId();
        SubCriterionEntity subCriterion = subCriterionRepository.findById(subCriterionId).get();
        teacherScoreEntity.setSubCriterion(subCriterion);
        teacherScoreRepository.save(teacherScoreEntity);
    }

    @Override
    public List<SearchResultDto> searchByFilter(SearchFilterDto searchFilterDto) {
        String firstName = searchFilterDto.getFirstName() == null ? null : "%" + searchFilterDto.getFirstName() + "%";
        String fathersName = searchFilterDto.getFathersName() == null ? null : "%" + searchFilterDto.getFathersName() + "%";
        String lastName = searchFilterDto.getLastName() == null ? null : "%" + searchFilterDto.getLastName() + "%";
        List<TeacherScoreEntity> foundEntities = teacherScoreRepository.findByFilter(firstName, fathersName, lastName,
                searchFilterDto.getSubCriterion(), searchFilterDto.getCriterion(), searchFilterDto.getCathedra(),
                searchFilterDto.getFaculty());
        return teacherScoreMapper.toSearchResultList(foundEntities);
    }
}
