package by.grsu.rating;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@RunWith(MockitoJUnitRunner.class)
public class TestClass {
    @Test
    public void test() {
        solution("996");
    }

    public Boolean solution(String num_str) {
        String reversed = new StringBuilder(num_str).reverse().toString();
        StringBuilder result = new StringBuilder();
        for (String s : reversed.split("")) {
            String ss = reverse(s);
            if (ss == null) {
                return false;
            }
            result.append(ss);
        }
        return num_str.equals(result.toString());
    }

    public String reverse(String num) {
        switch (num) {
            case "6":
                return "9";
            case "9":
                return "6";
            case "8":
            case "1":
            case "0":
                return num;
            default:
                return null;
        }
    }
}